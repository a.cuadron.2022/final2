from PIL import Image
from images import read_img, write_img,create_blank


def mirror(img):

  width = img["width"]
  height = img["height"]

  mirrored_image = create_blank(width, height)

  for y in range(height):
    for x in range(width):
        mirrored_index= y * width + x
        original_index = (height - 1 - y) * width + x
        mirrored_image['pixels'][mirrored_index] = img['pixels'][original_index]

  return mirrored_image

def grayscale(image):

  pixeles_original=image["pixels"]

  pixeles_gris = []

  for pixel in pixeles_original:
    red, green, blue = pixel
    grayscale = (red + green + blue) // 3

    pixeles_gris.append((grayscale, grayscale, grayscale))
    image["pixels"]=pixeles_gris

  return image


def blur(img):
    width = img['width']
    height = img['height']

    blurred = create_blank(width, height)

    for y in range(height):
        for x in range(width):

            red_sum = 0
            green_sum = 0
            blue_sum = 0
            count = 0

            for ky in range(-1, 2):
                for kx in range(-1, 2):

                    if y + ky < 0 or y + ky >= height:
                        continue

                    if x + kx < 0 or x + kx >= width:
                        continue

                    pixel = img['pixels'][(y + ky) * width + (x + kx)]

                    red_sum += pixel[0]
                    green_sum += pixel[1]
                    blue_sum += pixel[2]
                    count += 1

            blurred['pixels'][y * width + x] = (
                int(red_sum / count),
                int(green_sum / count),
                int(blue_sum / count))

    return blurred

def change_colors(img, original, change):

  if isinstance(original, str):
    original = [(int(c.split(",")[0]), int(c.split(",")[1]), int(c.split(",")[2])) for c in original.split(":")]

  if isinstance(change, str):
    change = [(int(c.split(",")[0]), int(c.split(",")[1]), int(c.split(",")[2])) for c in change.split(":")]

  width = img['width']
  height = img['height']

  change_img = create_blank(width, height)

  for y in range(height):
    for x in range(width):

      original_pixel = img['pixels'][y * width + x]
      new_pixel = original_pixel

      for i in range(len(original)):
        if original_pixel == original[i]:
          new_pixel = change[i]
          break

      change_img['pixels'][y * width + x] = new_pixel

  return change_img

def rotate(img: dict, direction: str) -> dict:
    width = img['width']
    height = img['height']

    rotated = create_blank(height, width)

    if direction == 'right':
        for x in range(width):
            for y in range(height):
                new_x = -y
                new_y = x
                rotated['pixels'][new_x + new_y * rotated['width']] = img['pixels'][y * width + x]

    elif direction == 'left':
        for x in range(img['width']):
            for y in range(img['height']):
                new_x = y
                new_y = -x
                rotated['pixels'][new_x + new_y * rotated['width']] = img['pixels'][y * width + x]

    return rotated

def shift(image, horizontal=0, vertical=0):
    width = image['width']
    height = image['height']

    new_width = width + horizontal
    new_height = height + vertical

    shifted = create_blank(new_width, new_height)

    for x in range(width):
        for y in range(height):
            new_x = x + horizontal
            new_y = y + vertical
            shifted['pixels'][new_y * new_width + new_x] = image['pixels'][y * width + x]

    return shifted

def crop(image, x, y, width, height):

    orig_width = image['width']
    orig_height = image['height']
    cropped_image = create_blank(width, height)

    for fila in range(height):

        for columna in range(width):

            cambiado_x = x + columna
            cambiado_y = y + fila

            original_index = cambiado_y * orig_width + cambiado_x
            recorte_index = fila * width + columna

            cropped_image['pixels'][recorte_index] = image['pixels'][original_index]

    return cropped_image

def filter(img, r, g, b):

    width = img['width']
    height = img['height']

    filter_image={'width':img['width'], 'height':img['height'], 'pixels':[]}

    for y in range(height):
        for x in range(width):

            pixel = img['pixels'][y * width + x]

            new_red = int(pixel[0] * r)
            new_green = int(pixel[1] * g)
            new_blue = int(pixel[2] * b)

            new_pixel = (new_red, new_green, new_blue)

            filter_image['pixels'].append(new_pixel)

    return filter_image

def sepia(img):
    width = img['width']
    height = img['height']

    image_sepia = create_blank(width, height)
    for pixel in range(0, width*height):
        r, g, b = img["pixels"][pixel]
        r, g, b = 0.393*r+0.769*g+0.189*b, 0.349*r+0.686*g+0.168*b, 0.272*r+0.534*g+0.131*b
        if r > 255:
            r = 255
        if g > 255:
            g = 255
        if b > 255:
            b = 255
        image_sepia["pixels"][pixel] = int(r), int(g), int(b)
    return image_sepia

def negative(img):

    width = img['width']
    height = img['height']

    image_negative = create_blank(width, height)
    for pixel in range(0, width*height):
        r, g, b = img["pixels"][pixel]
        r, g, b = 255-r, 255-g, 255-b
        image_negative["pixels"][pixel] = (r, g, b)
    return image_negative