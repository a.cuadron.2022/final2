ENTREGA CONVOCATORIA JUNIO

Nombre: Alejandro Cuadron Montserrat

Correo: a.cuadron.2022@alumnos.urjc.es

Enlace video: https://youtu.be/qc5_6AQBILQ

Requisitos mínimos cumplidos:

- Programa transforms.py  

    - Función Mirror 

    - Función Grayscale 

    - Función Blur 

    - Función Change_colors 

    - Función Rotate 

    - Función Shift 

    - Función Crop 

    - Función Filter 

- Programa transform_simple.py  

- Programa transform_args.py  

- Programa transform_multi.py

Requisitos opcionales cumplidos:

    - Función Sepia 

    - Función Negative